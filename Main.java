package reflection;




public class Main {
    public static void main(String[] args) {

        Main start = new Main();
        start.methodStart();
    }

    private void methodStart() {
        Character hero = Character.createCharacter("Static Shock", 26,76);

        ObjectDescriber objectDescriber = ObjectDescriber.getInstance();

        objectDescriber.describe(hero);

    }
}
