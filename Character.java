package reflection;

public class Character implements PlainOleJavaObject {
    private String name;
    private int age;
    private int height;

    public static Character createCharacter(String name,int age,  int height) {
        Character character = new Character();
        character.setAge(age);
        character.setHeight(height);
        character.setName(name);
        return character;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


}