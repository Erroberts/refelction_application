package reflection;


import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public final class ObjectDescriber {
    private static final ObjectDescriber object = new ObjectDescriber();

    private ObjectDescriber() {
    }

    public static ObjectDescriber getInstance() {

        return object;
    }

    public void describe(PlainOleJavaObject object) {
        Class clazz = object.getClass();
        String className = clazz.getName();
        System.out.println(className);
        int getModifier = clazz.getModifiers();
        System.out.println("Is it public: " + Modifier.isPublic(getModifier));
        Method[] classMethods = clazz.getMethods();
        for (Method method : classMethods) {
            System.out.println("Method Name: " + method.getName());
            if (method.getName().startsWith("get")) {
                System.out.println("this is a GETTER" + "\n");
            } else if (method.getName().startsWith("set")) {
                System.out.println("this is a SETTER" + "\n");
            }
            System.out.println("Return Type:" + method.getReturnType());
            Class[] parameterType = method.getParameterTypes();
            System.out.println("Parameters");
            for (Class parameter : parameterType) {
                System.out.println(parameter.getName() + "\n");

            }
        }
        try {
            Constructor constructor = null;
            Object box = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }


}
